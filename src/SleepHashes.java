import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.cli.*;
import java.awt.*;

class HashResult {
    String hash;
    String word;

    public HashResult(String h, String w) {
        this.hash = h;
        this.word = w;
    }
}

class SleepHashes {
    private static final int FNV1_32_INIT = 0x811c9dc5;
    private static final int FNV1_PRIME_32 = 16777619;

    private static Properties hashes = new Properties();

    public static void main(String[] args) {
        Options options = new Options();

        Option pathOption = new Option("p", "path", true, "path to prerendered sleepword pngs");
        pathOption.setRequired(true);
        pathOption.setArgs(1);
        options.addOption(pathOption);

        Option clientOption = new Option("c", "client", true, "target client for hashes (apos|idle)");
        clientOption.setRequired(true);
        clientOption.setArgs(1);
        options.addOption(clientOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("sleep-hash-gen", options);

            System.exit(1);
        }

        String client = cmd.getOptionValue("client");
        if (!client.equalsIgnoreCase("apos") && !client.equalsIgnoreCase("idle")) {
            System.out.println("--client value must be 'apos' or 'idle'");
            formatter.printHelp("sleep-hash-gen", options);

            System.exit(1);
        }


        String path = cmd.getOptionValue("path");
        boolean apos = client.equalsIgnoreCase("apos");

        System.out.println("Generating hashes for " + (apos ? "APOS" : "IdleRSC") + "...");
        GenerateHashes(path, apos);
        System.out.println("Saving hashes to file...");
        SaveHashes();
        System.out.println("Complete");
    }

    private static void SaveHashes() {
        try {
            String output = "./hashes.properties";
            Path path = Paths.get(output);
            Files.deleteIfExists(path);

            try (FileOutputStream outputStream = new FileOutputStream(output);) {
                hashes.store(outputStream, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void GenerateHashes(String path, boolean apos) {
        File sleepwordDataDir = new File(path);
        File[] sleepwordFiles = sleepwordDataDir.listFiles();
        if (sleepwordFiles == null) {
            // server owner doesn't have a sleepwords directory
            System.out.println("Could not access path to sleepwords.");

            System.exit(1);
            return;
        }
        for (File fname : sleepwordFiles) {
            HashResult hash = GenerateHash(fname, apos);
            if (hash == null)
                continue;
            hashes.put(hash.hash, hash.word);
        }
    }

    private static HashResult GenerateHash(File fname, boolean apos) {
        String correctWord = "-null-";
        boolean knowTheWord = false;

        int endOfWordIndex = fname.getName().indexOf('_', 6);
        if (endOfWordIndex < 6) {
            // filename parsed is not of the expected format. assuming all characters except
            // last 4 are the correct word.
            knowTheWord = true;
            correctWord = fname.getName().substring(0, fname.getName().length() - 4); // remove 4 character file
            // extension
        } else {
            // example filenames:
            // sleep_!INCORRECT!instar__flying sno_flying sno (redacted chat)
            // replays_bot204_dist_bot204_replay_penuslarge1_06-18-2018 06.23.47_455.png
            // sleep_crushing_Logg_Tylerbeg_06-13-2018 20.09.59 high alch from 55 to 60 and
            // I got a dmed lol_70.png
            // see:
            // https://github.com/2003scape/rsc-captcha-archives/tree/master/rscplus-replay-sleepwords

            String candidateWord = fname.getName().substring(6, fname.getName().indexOf('_', 6));
            knowTheWord = !(candidateWord.contains("!INCORRECT!") ||
                    candidateWord.contains("!SUDDENLY-AWOKE!"));
            if (knowTheWord) {
                correctWord = candidateWord;
            }
        }

        if (!knowTheWord)
            correctWord = "unknown";

        byte[] image;
        if (apos)
            image = GetImageData(fname);
        else
            image = readFull(fname);

        int hash = hash32(image);

        return new HashResult(Integer.toString(hash), correctWord);
    }

    private static byte[] GetImageData(File fname) {
        byte[] data = imageFileToRLE(fname);
        byte[] padded = new byte[6000];
        System.arraycopy(data, 0, padded, 1, data.length);
        byte[] image = convertImage(padded);
        return image;
    }

    /**
     * FNV1a 32 bit variant.
     *
     * @param data - input byte array
     * @return - hashcode
     */
    private static int hash32(byte[] data) {
        return hash32(data, data.length);
    }

    /**
     * FNV1a 32 bit variant.
     *
     * @param data   - input byte array
     * @param length - length of array
     * @return - hashcode
     */
    private static int hash32(byte[] data, int length) {
        int hash = FNV1_32_INIT;
        for (int i = 0; i < length; i++) {
            hash ^= (data[i] & 0xff);
            hash *= FNV1_PRIME_32;
        }

        return hash;
    }

    private static byte[] readFull(File f) {
        try {
            return Files.readAllBytes(f.toPath());
        } catch (Exception e) {
            return null;
        }
    }

    private static byte[] convertImage(final byte[] data) {
        int var1 = 1;
        byte var2 = 0;
        final byte[] var4 = new byte[10200];
        int var3;
        int var5;
        int var6;
        for (var3 = 0; var3 < 255; var2 = (byte) (255 - var2)) {
            var5 = data[var1++] & 255;
            for (var6 = 0; var6 < var5; ++var6) {
                var4[var3++] = var2;
            }
        }
        for (var5 = 1; var5 < 40; ++var5) {
            var6 = 0;
            while (var6 < 255) {
                final int var7 = data[var1++] & 255;
                for (int var8 = 0; var8 < var7; ++var8) {
                    var4[var3] = var4[var3 - 255];
                    ++var3;
                    ++var6;
                }
                if (var6 < 255) {
                    var4[var3] = (byte) (255 - var4[var3 - 255]);
                    ++var3;
                    ++var6;
                }
            }
        }
        return var4;
    }

    /**
     * Reads a compatible image format into one that can be displayed by
     * the authentic RSC235 client. PNG and BMP are known compatible.
     *
     * Images will be resized from any arbitrary resolution and
     * colour depth will be reduced to 1 bit.
     *
     * @param fname filename
     * @return run length encoded image byte data
     */
    private static byte[] imageFileToRLE(File fname) {
        return booleansToRLE(loadImageFileToBooleanImage(fname));
    }

    /**
     * @param fname filename
     * @return ██ █ ██ █ ██ █ █
     *         █ █ █ █ █ ██ ██ 255 x 40
     *         ███ ██ ███ ██ █ boolean style image
     */
    private static boolean[][] loadImageFileToBooleanImage(File fname) {
        int WIDTH = 255;
        int HEIGHT = 40;
        boolean[][] imageArray = new boolean[WIDTH][HEIGHT];

        try {
            BufferedImage image = resizeSanitize(ImageIO.read(fname));
            int imgHeight = image.getHeight();
            int imgWidth = image.getWidth();

            boolean drawToConsole = false; // fun option, but obviously not necessary. :-)

            for (int y = 0; y < imgHeight && y < HEIGHT; y += 1) {
                for (int x = 0; x < imgWidth && x < WIDTH; x += 1) {
                    imageArray[x][y] = image.getRGB(x, y) > -5000;

                    // disabled by default
                    if (drawToConsole) {
                        if (imageArray[x][y]) {
                            System.out.print("█");
                        } else {
                            System.out.print(" ");
                        }
                    }
                }
                if (drawToConsole)
                    System.out.println("/");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageArray;
    }

    /**
     * @param imageData in "custom" boolean[][] format
     * @return bytes that are safe to send to authentic client as image data
     */
    private static byte[] booleansToRLE(boolean[][] imageData) {
        int WIDTH = 255; // Warning: don't try to use the last column, it is buggy in authentic client
        int HEIGHT = 40;
        int x = 0;
        int y = 0;
        boolean lastColour = false;
        int length = 0;
        ArrayList<Byte> image = new ArrayList<Byte>();

        // first row uses RLE horizontally
        // whatever colour is in the last pixel of the row will be used for that entire
        // column
        for (; x < WIDTH; x += 1) {
            if (imageData[x][y] == lastColour) {
                length += 1;
            } else {
                image.add((byte) length);
                length = 1;
                lastColour = !lastColour;
            }
        }
        image.add((byte) length);

        // subsequent rows look at the pixel above and use RLE vertically
        for (y = 1; y < HEIGHT; y += 1) {
            length = 0;
            for (x = 0; x < WIDTH; x += 1) {
                if (imageData[x][y] == imageData[x][y - 1]) {
                    length += 1;
                } else {
                    image.add((byte) length);
                    length = 0;
                }
            }
            image.add((byte) length);
        }

        Byte[] imageBytes = new Byte[image.size()];
        imageBytes = image.toArray(imageBytes);
        return ArrayUtils.toPrimitive(imageBytes);
    }

    // ensure image can be displayed at 255x40 or fall back to 254x40 scaled
    private static BufferedImage resizeSanitize(BufferedImage img) {
        int imgHeight = img.getHeight();
        int imgWidth = img.getWidth();
        int buggyColumn = 254;

        // if image is approximately the correct size, won't scale image & will just
        // truncate to top left corner
        // but we must correct column 255 (the last column) so it doesn't change in
        // value from top row
        if (imgWidth <= 260 && imgHeight <= 45) {
            if (imgWidth <= buggyColumn) {
                return img;
            }
            int topRowColour = img.getRGB(buggyColumn, 0);
            for (int y = 1; y < imgHeight; y++) {
                img.setRGB(buggyColumn, y, topRowColour);
            }
            return img;
        } else {
            // image is grossly large & we would like to scale/stretch it down.
            int newWidth = buggyColumn; // limits image to before the buggy column
            int newHeight = 40;

            Image tmp = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
            BufferedImage dimg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2d = dimg.createGraphics();
            g2d.drawImage(tmp, 0, 0, null);
            g2d.dispose();

            return dimg;
        }
    }
}